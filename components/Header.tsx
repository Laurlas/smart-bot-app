import React from 'react'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { TouchableOpacity, View } from 'react-native'

export const Header = ({navigation}: any) => {
  const ICON_SIZE = 50
  return (
    <View
      style={{
        backgroundColor: '#000',
        width: '100%',
        display: 'flex'
      }}>
      <View style={{
        width: '100%',
        paddingLeft: 15,
        paddingRight: 15
      }}>
        <View style={{
          display: 'flex',
          height: ICON_SIZE,
          width: '100%',
          zIndex: 100,
          flexDirection: 'row'
        }}>
          <TouchableOpacity
            activeOpacity={0.6}
            style={{width: 50, alignSelf: 'center', zIndex: 10}}
            onPress={() => navigation.openDrawer()}>
            <MaterialCommunityIcons name={'menu'} size={28} color={'#fff'}/>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}