import React from 'react'
import {
  CREATE_BOT, CREATE_BOT_FAIL,
  CREATE_BOT_SUCCESS, DELETE_BOT, DELETE_BOT_FAIL,
  DELETE_BOT_SUCCESS,
  GET_BOT_SUCCESS,
  GET_BOTS_SUCCESS, GET_MARKETS_HOLDINGS, GET_MARKETS_HOLDINGS_FAIL, GET_MARKETS_HOLDINGS_SUCCESS, GET_MARKETS_SUCCESS
} from './BotsActions'

const initialState = {
  bots: [],
  botsMap: {},
  markets: [],
  balances: null,
  minimums: {},
  loading: false
}

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case GET_BOTS_SUCCESS:
      return {
        ...state,
        bots: action.payload
      }
    case DELETE_BOT:
      return {
        ...state,
        loading: true
      }
    case DELETE_BOT_FAIL:
      return {
        ...state,
        loading: false
      }
    case DELETE_BOT_SUCCESS:
      const toDeleteIndex = state.bots.findIndex((bot: any) => bot.id === action.payload)
      if (toDeleteIndex > -1) {
        state.bots.splice(toDeleteIndex, 1)
      }
      return {
        ...state,
        bots: [
          ...state.bots
        ],
        loading: false
      }
    case CREATE_BOT:
      return {
        ...state,
        loading: true
      }
    case CREATE_BOT_FAIL:
      return {
        ...state,
        loading: false
      }
    case CREATE_BOT_SUCCESS:
      return {
        ...state,
        bots: [
          ...state.bots,
          action.payload
        ],
        loading: false
      }
    case GET_BOT_SUCCESS:
      return {
        ...state,
        botsMap: {
          ...state.botsMap,
          [action.payload.id]: action.payload.bot
        }
      }
    case GET_MARKETS_SUCCESS:
      return {
        ...state,
        markets: action.payload.markets || [],
        minimums: action.payload.minimums || {}
      }
    case GET_MARKETS_HOLDINGS:
      return {
        ...state,
        loading: state.balances === null
      }
    case GET_MARKETS_HOLDINGS_SUCCESS:
      return {
        ...state,
        balances: action.payload.balances,
        loading: false
      }
    case GET_MARKETS_HOLDINGS_FAIL:
      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}
const BotsContext = React.createContext({})

const BotsContextProvider = (props: any) => {
  const [state, dispatch] = React.useReducer(reducer, initialState)
  return (
    <BotsContext.Provider value={{state, dispatch}}>
      {props.children}
    </BotsContext.Provider>
  )
}


export { BotsContext, BotsContextProvider }
