import { API_URL } from '../../config/constants'
import { makeAuthRequest } from '../../helpers/requestHelpers'

export const GET_BOTS_SUCCESS = 'GET_BOTS_SUCCESS'
export const GET_BOT_SUCCESS = 'GET_BOT_SUCCESS'
export const CREATE_BOT = 'CREATE_BOT'
export const CREATE_BOT_FAIL = 'CREATE_BOT_FAIL'
export const CREATE_BOT_SUCCESS = 'CREATE_BOT_SUCCESS'
export const DELETE_BOT = 'DELETE_BOT'
export const DELETE_BOT_FAIL = 'DELETE_BOT_FAIL'
export const DELETE_BOT_SUCCESS = 'DELETE_BOT_SUCCESS'
export const GET_MARKETS_SUCCESS = 'GET_MARKETS_SUCCESS'
export const GET_MARKETS_HOLDINGS = 'GET_MARKETS_HOLDINGS'
export const GET_MARKETS_HOLDINGS_SUCCESS = 'GET_MARKETS_HOLDINGS_SUCCESS'
export const GET_MARKETS_HOLDINGS_FAIL = 'GET_MARKETS_HOLDINGS_FAIL'

export const getBots = async (authContext: any, botsContext: any) => {
  const response = await makeAuthRequest({
      url: `${API_URL}/bots`,
      method: 'get'
    },
    undefined,
    undefined
  )(authContext)
  if (response && response.data) {
    botsContext.dispatch({
      type: GET_BOTS_SUCCESS,
      payload: response.data
    })
  }
}
export const getBot = async (authContext: any, botsContext: any, id: string, dateInterval: string) => {
  const response = await makeAuthRequest({
      url: `${API_URL}/bots/${id}`,
      method: 'get',
      params: {dateInterval}
    },
    undefined,
    undefined
  )(authContext)
  if (response && response.data) {
    botsContext.dispatch({
      type: GET_BOT_SUCCESS,
      payload: {id, bot: response.data}
    })
  }
}
export const createBot = async (authContext: any, botsContext: any, data: any, navigation: any) => {
  botsContext.dispatch({
    type: CREATE_BOT
  })
  const response = await makeAuthRequest({
      url: `${API_URL}/bots`,
      method: 'post',
      data
    },
    undefined,
    undefined
  )(authContext)
  if (response && response.data) {
    botsContext.dispatch({
      type: CREATE_BOT_SUCCESS,
      payload: response.data
    })
    navigation.navigate('BotInfo', {id: response.data.id})
  } else {
    botsContext.dispatch({
      type: CREATE_BOT_FAIL
    })
  }
}
export const removeBot = async (authContext: any, botsContext: any, id: string, data: any, navigation: any) => {
  botsContext.dispatch({
    type: DELETE_BOT,
    payload: id
  })
  const response = await makeAuthRequest({
      url: `${API_URL}/bots/${id}`,
      method: 'delete',
      data
    },
    undefined,
    undefined
  )(authContext)
  if (response && response.data) {
    botsContext.dispatch({
      type: DELETE_BOT_SUCCESS,
      payload: id
    })
    navigation.navigate('Bots', {id: response.data.id})
  } else {
    botsContext.dispatch({
      type: DELETE_BOT_FAIL,
      payload: id
    })
  }
}
export const getMarkets = async (authContext: any, botsContext: any) => {
  const response = await makeAuthRequest({
      url: `${API_URL}/markets`,
      method: 'get'
    },
    undefined,
    undefined
  )(authContext)
  if (response && response.data) {
    botsContext.dispatch({
      type: GET_MARKETS_SUCCESS,
      payload: response.data
    })
  }
}
export const getMarketsHoldings = async (authContext: any, botsContext: any) => {
  botsContext.dispatch({
    type: GET_MARKETS_HOLDINGS
  })
  const response = await makeAuthRequest({
      url: `${API_URL}/markets/holdings`,
      method: 'get'
    },
    undefined,
    undefined
  )(authContext)
  if (response && response.data) {
    botsContext.dispatch({
      type: GET_MARKETS_HOLDINGS_SUCCESS,
      payload: response.data
    })
  } else {
    botsContext.dispatch({
      type: GET_MARKETS_HOLDINGS_FAIL
    })
  }
}