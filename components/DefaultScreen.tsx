import React, { useContext } from 'react'
import {
  Dimensions,
  Keyboard,
  Platform,
  SafeAreaView, ScrollView,
  StatusBar,
  StyleSheet,
  TouchableWithoutFeedback,
  View
} from 'react-native'
import { Loader } from './partials/Loader'
import { AuthContext } from './Auth/AuthContext'
import { BotsContext } from './Bots/BotsContext'
import { Header } from './Header'

export const DefaultScreen = ({children, style, navigation}: any) => {
  const authContext = useContext(AuthContext) as any
  const botsContext = useContext(BotsContext) as any
  const loading = authContext?.state?.loading || botsContext?.state?.loading
  let baseStyle = {
    backgroundColor: '#F6F6F6',
    flex: 1
  }
  if (style) {
    baseStyle = {
      ...baseStyle,
      ...style
    }
  }
  return (
    <>
      <MyStatusBar backgroundColor={'#000'} barStyle="light-content"/>
      <SafeAreaView style={baseStyle}>
        <Header navigation={navigation}/>
        <Loader visible={loading}/>
        <ScrollView>
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <View style={{flex: 1, width: '100%'}}>
              {
                children
              }
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      </SafeAreaView>
    </>
  )
}
export const MyStatusBar = ({backgroundColor}: any) => (
  <SafeAreaView style={[styles.statusBar, {position: 'relative'}, {backgroundColor}]}>
    <StatusBar translucent barStyle="light-content" backgroundColor={'transparent'}/>
  </SafeAreaView>
)

const X_WIDTH = 375
const X_HEIGHT = 812

const XSMAX_WIDTH = 414
const XSMAX_HEIGHT = 896

const {height, width} = Dimensions.get('window')

export const isIPhoneX = () => Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS
  ? width === X_WIDTH && height === X_HEIGHT || width === XSMAX_WIDTH && height === XSMAX_HEIGHT
  : false

export const StatusBarHeight = Platform.select({
  ios: isIPhoneX() ? 44 : 24,
  android: StatusBar.currentHeight,
  default: 0
})

const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    height: StatusBarHeight
  },
  appBar: {
    backgroundColor: '#79B45D',
    height: APPBAR_HEIGHT
  },
  content: {
    flex: 1,
    backgroundColor: '#33373B'
  }
})
