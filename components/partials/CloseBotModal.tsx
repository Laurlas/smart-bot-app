import { Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import * as React from 'react'
import { removeBot } from '../Bots/BotsActions'
import { useContext } from 'react'
import { AuthContext } from '../Auth/AuthContext'
import { BotsContext } from '../Bots/BotsContext'

export const CloseBotModal = ({modalVisible, setModalVisible, botInfo, navigation, id}: any) => {
  const authContext = useContext(AuthContext) as any
  const botsContext = useContext(BotsContext) as any
  return (
    <View
      style={styles.centeredView}
    >
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible)
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>You are about to close your {botInfo.market} bot. What do you want to do with
              your remaining {botInfo.market}?</Text>
            <TouchableOpacity
              onPress={() => {
                removeBot(authContext, botsContext, id, {sell: true}, navigation)
                setModalVisible(false)
              }}
              activeOpacity={0.8}
              style={styles.button}
            >
              <Text style={styles.buttonText}>Sell to USDT</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                removeBot(authContext, botsContext, id, {sell: true}, navigation)
                setModalVisible(false)
              }}
              activeOpacity={0.8}
              style={styles.button}
            >
              <Text style={styles.buttonText}>Keep</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(false)
              }}
              activeOpacity={0.8}
              style={styles.button}
            >
              <Text style={styles.buttonText}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  )
}
const styles = StyleSheet.create({
  centeredView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
    flex: 1
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: '100%'
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 18
  },
  button: {
    backgroundColor: '#000',
    height: 50,
    width: '100%',
    padding: 10,
    borderRadius: 10,
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 20
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center'
  }
})