import React from 'react'
import { StyleSheet, Text } from 'react-native'
import AnimatedLoader from 'react-native-animated-loader'
import * as cryptoLottie from '../../assets/lottie/14117-crypto-loader.json'

export const Loader = ({visible = false, label = 'Working on it..'}: any) => (
  <AnimatedLoader
    visible={visible}
    overlayColor="rgba(0,0,0,0.32)"
    source={cryptoLottie}
    animationStyle={styles.lottie}
    speed={1}
  >
    <Text style={{color: '#fff'}}>{label}</Text>
  </AnimatedLoader>
)

const styles = StyleSheet.create({
  lottie: {
    width: 100,
    height: 100
  }
})