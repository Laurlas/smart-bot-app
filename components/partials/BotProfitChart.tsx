import * as React from 'react'
import { Dimensions, Text, View } from 'react-native'
import { Picker } from '@react-native-community/picker'
import { LineChart } from 'react-native-chart-kit'
import { useMemo } from 'react'
import moment from 'moment'

const formatDbMapper = {
  '%d-%m-%Y': 'DD-MM-YYYY',
  '%m-%Y': 'MM-YYYY',
  '%Y': 'YYYY'
} as any
const formatMapper = {
  '%d-%m-%Y': 'DD MMM',
  '%m-%Y': 'MMM YYYY',
  '%Y': 'YYYY'
} as any
export const BotProfitChart = ({profitStats, dateInterval, setDateInterval}: any) => {
  const {labels, data} = useMemo(() => {
    const labels = profitStats.map((stat: any) => moment(stat.fillDate, formatDbMapper[dateInterval]).format(formatMapper[dateInterval]))
    const data = profitStats.map((stat: any) => stat.percentageProfit)
    return {labels, data}
  }, [profitStats])
  return (
    <View>
      <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
        <Text style={{fontSize: 16}}>Profit Chart (%)</Text>
        <Picker
          selectedValue={dateInterval}
          style={{height: 50, width: 130}}
          onValueChange={(itemValue) => {
            setDateInterval(itemValue)
          }}
        >
          <Picker.Item label="Daily" value="%d-%m-%Y"/>
          <Picker.Item label="Monthly" value="%m-%Y"/>
          <Picker.Item label="Yearly" value="%Y"/>
        </Picker>
      </View>
      <LineChart
        data={{
          labels,
          datasets: [
            {
              data
            }
          ]
        }}
        width={Dimensions.get('window').width - 30}
        height={220}
        yAxisSuffix="%"
        yAxisInterval={1}
        chartConfig={{
          backgroundColor: '#2a61ec',
          backgroundGradientFrom: '#2a61ec',
          backgroundGradientTo: '#5686f8',
          decimalPlaces: 2,
          color: (opacity = 1) => `rgba(255, 255, 255, 0.5)`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, 1)`,
          style: {
            borderRadius: 16
          },
          propsForDots: {
            r: '6',
            strokeWidth: '2',
            stroke: '#5686f8'
          },
          fillShadowGradientOpacity: 0.5
        }}
        style={{
          marginVertical: 8,
          borderRadius: 16
        }}
        bezier
      />
    </View>
  )
}