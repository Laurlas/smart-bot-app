import * as React from 'react'
import { Dimensions, Text, View } from 'react-native'
import { BarChart } from 'react-native-chart-kit'

export const BotHoldingsChart = ({labels, data}: any) => {
  return (
    <View>
      <BarChart
        style={{
          marginVertical: 8,
          borderRadius: 16
        }}
        data={{
          labels,
          datasets: [{
            data
          }]
        }}
        width={Dimensions.get('window').width - 30}
        height={220}
        yAxisLabel={''}
        yAxisSuffix={''}
        xAxisLabel={''}
        chartConfig={{
          backgroundColor: '#2a61ec',
          backgroundGradientFrom: '#2a61ec',
          backgroundGradientTo: '#5686f8',
          decimalPlaces: 2,
          color: (opacity = 1) => `rgba(255, 255, 255, 0.5)`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, 1)`,
          style: {
            borderRadius: 16
          },
          propsForDots: {
            r: '6',
            strokeWidth: '2',
            stroke: '#ffa726'
          },
          fillShadowGradientOpacity: 0.5
        }}
      />
    </View>
  )
}