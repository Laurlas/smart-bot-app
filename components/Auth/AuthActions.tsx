import axios from 'axios'
import { API_URL, clientConfig } from '../../config/constants'
import { makeAuthRequest, REQUEST_AUTH_TOKEN, REQUEST_AUTH_TOKEN_FAILED } from '../../helpers/requestHelpers'
import { Alert, Platform } from 'react-native'

export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS'

export const login = async (authContext: any, email: string, password: string) => {
  let response
  try {
    response = await axios({
        url: `${API_URL}/oauth2/token`,
        method: 'post',
        data: {
          ...clientConfig,
          username: email,
          password: password
        }
      }
    )
  } catch (err) {
    setTimeout(() => {
      Alert.alert('Error', 'Invalid email or password')
    }, 100)
    authContext.dispatch({
      type: REQUEST_AUTH_TOKEN_FAILED,
      payload: err.response.data
    })
  }
  if (response) {
    authContext.dispatch({
      type: REQUEST_AUTH_TOKEN,
      payload: response.data
    })
  }
}
export const logout = async ({authContext}: any) => {
  await makeAuthRequest({
      url: `${API_URL}/oauth2/logout`,
      method: 'post'
    },
    undefined,
    undefined
  )(authContext)
  authContext.dispatch({
    type: LOGOUT_SUCCESS
  })
}
export const getUser = async (authContext: any) => {
  const response = await makeAuthRequest({
      url: `${API_URL}/users/me`,
      method: 'get'
    },
    undefined,
    undefined
  )(authContext)
  if (response && response.data) {
    authContext.dispatch({
      type: GET_USER_SUCCESS,
      payload: response.data
    })
  }
}

export const UPDATE_LOGGED_IN_USER_DETAILS = 'UPDATE_LOGGED_IN_USER_DETAILS'
export const UPDATE_LOGGED_IN_USER_DETAILS_SUCCESS = 'UPDATE_LOGGED_IN_USER_DETAILS_SUCCESS'
export const UPDATE_LOGGED_IN_USER_DETAILS_FAIL = 'UPDATE_LOGGED_IN_USER_DETAILS_FAIL'

export const updateLoggedInUserDetails = async ({data, authContext, onSuccess}: any) => {
  authContext.dispatch({
    type: UPDATE_LOGGED_IN_USER_DETAILS
  })
  const response = await makeAuthRequest({
      url: `${API_URL}/users/me`,
      method: 'put',
      data
    },
    undefined,
    undefined
  )(authContext)
  if (response && onSuccess) {
    onSuccess()
  }
  response && authContext.dispatch({
    type: UPDATE_LOGGED_IN_USER_DETAILS_SUCCESS,
    payload: response.data
  })
  if (response) {
    setTimeout(() => {
      response && Alert.alert('Success', 'User Updated')
    }, 100)
  }
  !response && authContext.dispatch({
    type: UPDATE_LOGGED_IN_USER_DETAILS_FAIL
  })
}
export const registerNotificationToken = async ({authContext, token}: any) => {
  await makeAuthRequest({
      url: `${API_URL}/users/notification-token/${Platform.OS}/${token}`,
      method: 'post'
    },
    undefined,
    undefined
  )(authContext)
}

export const CREATE_USER = 'create_user'
export const CREATE_USER_SUCCESS = 'create_user_success'
export const CREATE_USER_FAIL = 'create_user_fail'

export const register = async ({authContext, data}: any) => {
  authContext.dispatch({
    type: CREATE_USER
  })
  let response
  try {
    response = await axios({
        url: `${API_URL}/users`,
        method: 'post',
        data,
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )

  } catch (e) {
    const error = e?.response?.data?.errors?.message || 'Something went wrong'

    setTimeout(() => {
      Alert.alert('Error', error)
    }, 100)
    authContext.dispatch({
      type: CREATE_USER_FAIL,
      payload: {}
    })
  }

  response && authContext.dispatch({
    type: CREATE_USER_SUCCESS,
    payload: {...response.data}
  })
  response && login(authContext, data.user.email, data.user.password)
  !response && authContext.dispatch({
    type: CREATE_USER_FAIL,
    payload: {}
  })
  return response
}