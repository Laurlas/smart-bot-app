import { StatusBar } from 'expo-status-bar'
import React, { useContext, useEffect, useRef, useState } from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context'

import useCachedResources from './hooks/useCachedResources'
import useColorScheme from './hooks/useColorScheme'
import Navigation from './navigation'
import { AuthContextProvider } from './components/Auth/AuthContext'
import { BotsContextProvider } from './components/Bots/BotsContext'

export default function App() {
  const isLoadingComplete = useCachedResources()
  const colorScheme = useColorScheme()

  if (!isLoadingComplete) {
    return null
  } else {
    return (
      <AuthContextProvider>
        <BotsContextProvider>
          <SafeAreaProvider>
            <Navigation colorScheme={colorScheme}/>
            <StatusBar/>
          </SafeAreaProvider>
        </BotsContextProvider>
      </AuthContextProvider>
    )
  }
}
