import * as React from 'react'
import { Alert, Keyboard, Text, TextInput, TouchableOpacity, View } from 'react-native'

import { DefaultScreen } from '../components/DefaultScreen'
import { Header } from '../components/Header'
import { getUser, login, updateLoggedInUserDetails } from '../components/Auth/AuthActions'
import { useContext, useEffect, useState } from 'react'
import { AuthContext } from '../components/Auth/AuthContext'

export default function ApiSettingsScreen({navigation}: any) {
  const [apiKey, setApiKey] = useState('')
  const [secretKey, setSecretKey] = useState('')
  const authContext = useContext(AuthContext) as any
  const onChangeApiKey = (newKey: string) => {
    setApiKey(newKey)
  }
  const onChangeApiSecret = (newSecret: string) => {
    setSecretKey(newSecret)
  }
  useEffect(() => {
    getUser(authContext)
  }, [])
  useEffect(() => {
    setApiKey(authContext.state.user.binanceApiKey)
    setSecretKey(authContext.state.user.binanceSecretKey)
  }, [authContext.state.user])
  return (
    <DefaultScreen navigation={navigation}>
      <View
        style={{
          padding: 15,
          flex: 1
        }}>
        <Text
          style={{
            fontSize: 24,
            padding: 10,
            fontWeight: 'bold',
            marginBottom: 15
          }}
        >
          Update Binance Api Settings
        </Text>
        <TextInput
          placeholder={'API Key'}
          value={apiKey}
          autoCapitalize={'none'}
          autoCorrect={false}
          onChangeText={onChangeApiKey}
          returnKeyType="send"
          textContentType="oneTimeCode"
          style={{
            height: 50,
            width: '100%',
            color: '#000',
            borderWidth: 1,
            padding: 10,
            borderRadius: 10
          }}
        />
        <TextInput
          placeholder={'Secret Key'}
          value={secretKey}
          autoCapitalize={'none'}
          autoCorrect={false}
          onChangeText={onChangeApiSecret}
          returnKeyType="send"
          textContentType="oneTimeCode"
          style={{
            height: 50,
            width: '100%',
            color: '#000',
            borderWidth: 1,
            padding: 10,
            borderRadius: 10,
            marginTop: 10
          }}
        />
        <TouchableOpacity
          onPress={() => {
            Keyboard.dismiss();
            updateLoggedInUserDetails({data: {user: {binanceApiKey: apiKey, binanceSecretKey: secretKey}}, authContext})

          }}
          activeOpacity={0.8}
          style={{
            backgroundColor: '#000',
            height: 50,
            width: '100%',
            padding: 10,
            borderRadius: 10,
            justifyContent: 'center',
            flexDirection: 'row',
            marginTop: 10
          }}
        >
          <Text style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
            alignSelf: 'center'
          }}>Submit</Text>
        </TouchableOpacity>
        {
          !authContext.state?.user?.validBinanceKey &&
          <Text style={{textAlign: 'center', marginTop: 15, marginBottom: 15, color: 'red'}}>Your binance settings are
            invalid. Please
            update them in order to use this app.</Text>
        }
      </View>
    </DefaultScreen>
  )
}