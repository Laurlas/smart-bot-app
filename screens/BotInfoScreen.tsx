import * as React from 'react'
import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
import { DefaultScreen } from '../components/DefaultScreen'
import { useContext, useEffect, useMemo, useRef, useState } from 'react'
import { AuthContext } from '../components/Auth/AuthContext'
import { BotsContext } from '../components/Bots/BotsContext'
import { getBot } from '../components/Bots/BotsActions'
import { CloseBotModal } from '../components/partials/CloseBotModal'
import { useFocusEffect } from '@react-navigation/native'
import { BotProfitChart } from '../components/partials/BotProfitChart'

export default function BotInfoScreen({navigation, route}: any) {
  const authContext = useContext(AuthContext) as any
  const botsContext = useContext(BotsContext) as any
  const [modalVisible, setModalVisible] = useState<boolean>(false)
  const [dateInterval, setDateInterval] = useState<string>('%d-%m-%Y')

  const {state: {botsMap}} = botsContext
  const {id} = route.params
  const botInfo = botsMap[id]
  const priceRef = useRef() as any
  useEffect(() => {
    priceRef.current = botInfo?.price
  })
  const color = botInfo?.price > priceRef?.current ? '#1ea31e' : '#cd0b0b'
  useFocusEffect(
    React.useCallback(() => {
      getBot(authContext, botsContext, id, dateInterval)
      const timer = setInterval(() => {
        getBot(authContext, botsContext, id, dateInterval)
      }, 5000)
      return () => {
        clearInterval(timer)
      }
    }, [dateInterval]))
  const {
    buyOrders, sellOrders
  } = useMemo(() => {
    if (!botInfo) {
      return {buyOrders: [], sellOrders: []}
    }
    const buyOrders = botInfo.orders.filter((order: any) => order.side === 'BUY').sort((a: any, b: any) => b.price.localeCompare(a.price))
    const sellOrders = botInfo.orders.filter((order: any) => order.side === 'SELL').sort((a: any, b: any) => a.price.localeCompare(b.price))
    return {buyOrders, sellOrders}
  }, [botInfo])
  return (
    <DefaultScreen navigation={navigation}>
      {
        botInfo ?
          <ScrollView
            style={{
              padding: 15,
              flex: 1
            }}>
            <Text
              style={{
                fontSize: 24,
                padding: 10,
                textAlign: 'center',
                fontWeight: 'bold'
              }}
            >
              {botInfo.market} bot
            </Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-around', padding: 30}}>
              <Text style={{fontSize: 16, alignSelf: 'center'}}>Current Price:</Text>
              <Text style={{
                fontSize: 20,
                fontWeight: 'bold',
                alignSelf: 'center',
                color
              }}>{Number(botInfo.price).toFixed(4)} USDT</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10}}>
              <Text style={{color: '#1ea31e'}}>Buy price</Text>
              <Text style={{color: '#000'}}>How far too fill</Text>
              <Text style={{color: '#cd0b0b'}}>Sell price</Text>
            </View>
            <View style={{flexDirection: 'row', flex: 1}}>
              <View style={{flexDirection: 'column', paddingRight: 10, flex: 1}}>
                {
                  buyOrders.length === 0 &&
                  <Text style={{alignSelf: 'center'}}>No buy orders</Text>
                }
                {
                  buyOrders.map((buyOrder: any, index: number) => <BuyOrder price={botInfo.price} order={buyOrder}
                                                                            key={index} index={index}/>
                  )
                }
              </View>
              <View style={{flexDirection: 'column', paddingLeft: 10, flex: 1}}>
                {
                  sellOrders.length === 0 &&
                  <Text style={{alignSelf: 'center'}}>No sell orders</Text>
                }
                {
                  sellOrders.map((buyOrder: any, index: number) => <SellOrder price={botInfo.price} order={buyOrder}
                                                                              key={index}
                                                                              index={index}/>
                  )
                }
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(true)
              }}
              activeOpacity={0.8}
              style={{
                backgroundColor: '#000',
                height: 50,
                width: '100%',
                padding: 10,
                borderRadius: 10,
                justifyContent: 'center',
                flexDirection: 'row',
                marginTop: 50,
                marginBottom: 30
              }}
            >
              <Text style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: 'bold',
                alignSelf: 'center'
              }}>Stop bot</Text>
            </TouchableOpacity>

            <View style={{flexDirection: 'row', justifyContent: 'space-around', padding: 30}}>
              <Text style={{fontSize: 16, alignSelf: 'center'}}>Total Profit:</Text>
              <Text style={{
                fontSize: 20,
                fontWeight: 'bold',
                alignSelf: 'center',
                color: botInfo?.totalProfitStats?.profit > 0 ? '#1ea31e' : '#cd0b0b'
              }}>{Number(botInfo?.totalProfitStats?.profit).toFixed(2)} USDT
                ({Number(botInfo?.totalProfitStats?.percentageProfit).toFixed(2)}%)</Text>
            </View>
            {
              botInfo.profitStats.length > 0 &&
              <BotProfitChart profitStats={botInfo.profitStats} dateInterval={dateInterval}
                              setDateInterval={setDateInterval}/>
            }
            <CloseBotModal botInfo={botInfo} modalVisible={modalVisible} setModalVisible={setModalVisible}
                           navigation={navigation} id={id}/>
          </ScrollView>
          :
          <>
          </>
      }
    </DefaultScreen>
  )
}

function BuyOrder({order, index, price}: any) {
  const fill = (price - order.price) * 100 / price
  return <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5}}>
    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
      <Text style={{
        marginRight: 10,
        color: '#fff',
        backgroundColor: '#1ea31e',
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 5,
        fontSize: 10,
        alignSelf: 'center'
      }}>{index + 1}</Text>
      <Text style={{color: '#000'}}>{Number(order.price).toFixed(4)}</Text>
    </View>
    <Text style={{color: '#000'}}>-{Number(fill).toFixed(2)}%</Text>
  </View>
}

function SellOrder({order, index, price}: any) {
  const fill = (order.price - price) * 100 / price
  return <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5}}>
    <Text style={{color: '#000'}}>+{Number(fill).toFixed(2)}%</Text>
    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
      <Text style={{color: '#000'}}>{Number(order.price).toFixed(4)}</Text>
      <Text style={{
        marginLeft: 10,
        color: '#fff',
        backgroundColor: '#cd0b0b',
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 5,
        fontSize: 10,
        alignSelf: 'center'
      }}>{index + 1}</Text>
    </View>
  </View>
}