import * as React from 'react'
import { View } from 'react-native'

import { DefaultScreen } from '../components/DefaultScreen'
import { Header } from '../components/Header'

export default function TabOneScreen({navigation}: any) {
  return (
    <DefaultScreen navigation={navigation}>
      <View
        style={{
          padding: 15,
          flex: 1
        }}>
      </View>
    </DefaultScreen>
  )
}