import * as React from 'react'
import { Alert, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { DefaultScreen } from '../components/DefaultScreen'
import { Header } from '../components/Header'
import { useContext, useEffect, useMemo, useState } from 'react'
import { AuthContext } from '../components/Auth/AuthContext'
import { BotsContext } from '../components/Bots/BotsContext'
import SearchablePicker from '../components/partials/SearchablePicker'
import { createBot, getMarkets } from '../components/Bots/BotsActions'
import { Loader } from '../components/partials/Loader'

export default function CreateBotScreen({navigation}: any) {
  const authContext = useContext(AuthContext) as any
  const botsContext = useContext(BotsContext) as any
  const [market, setMarket] = useState('')
  const [marketChanged, setMarketChanged] = useState(false)
  const [validMarket, setValidMarket] = useState(false)
  const [validInvestment, setValidInvestment] = useState(true)
  const [investment, setInvestment] = useState('100')
  const [ordersDistance, setOrdersDistance] = useState('2')
  const [sellProfit, setSellProfit] = useState('2')
  const [validOrdersDistance, setValidOrdersDistance] = useState(true)
  const [validSellProfit, setValidSellProfit] = useState(true)
  const {state: {markets, minimums}} = botsContext
  const marketData = useMemo(() => Object.entries(markets).map(([market]) => ({
    label: market,
    value: market
  })), [markets])
  const selectHandler = (item: any) => {
    setMarket(item.value)
    setMarketChanged(true)
    setValidMarket(!!(marketData.find((market: any) => market.value === item.value)))
  }
  const marketMinimums = minimums[market] || {}
  const toNumber = (value: string) => {
    return value.replace(/[^\d\-.,]/g, '')
      .replace(/,/g, '.')
      .replace(/\.(?=.*\.)/g, '')
  }
  const onChangeInvestment = (value: string) => {
    setInvestment((~~(value.replace(/[^0-9]/g, ''))).toString())
    setValidInvestment(!!value)
  }
  const onChangeOrdersDistance = (value: string) => {
    setOrdersDistance(toNumber(value))
    setValidOrdersDistance(!!value)
  }
  const onChangeSellProfit = (value: string) => {
    setSellProfit(toNumber(value))
    setValidSellProfit(!!value)
  }
  useEffect(() => {
    getMarkets(authContext, botsContext)
  }, [])
  if (marketData.length === 0) {
    return <Loader visible={true} label={'Just a sec..'}/>
  }
  return (
    <DefaultScreen navigation={navigation}>
      <View
        style={{
          padding: 15,
          flex: 1
        }}>
        <Text
          style={{
            fontSize: 24,
            padding: 10,
            textAlign: 'center',
            fontWeight: 'bold'
          }}
        >
          New bot
        </Text>
        <Text style={{marginTop: 10}}>Market</Text>
        <SearchablePicker
          onSelect={selectHandler}
          data={marketData}
          placeholder="Choose a market"
          inputStyles={{
            height: 50,
            width: '100%',
            borderColor: !marketChanged || validMarket ? '#000' : 'red',
            color: '#000',
            borderWidth: 1,
            padding: 10,
            borderRadius: 10
          }}
          containerStyles={{
            width: '100%',
            color: '#000',
            borderRadius: 10, zIndex: 2
          }}
          listStyles={{maxHeight: 200, backgroundColor: '#fff'}}
          defaultValue={''}
        />
        <Text style={{marginTop: 10}}>USDT Investment</Text>
        <TextInput
          placeholder={'USDT Investment'}
          value={investment}
          keyboardType="numeric"
          maxLength={8}
          autoCapitalize={'none'}
          autoCorrect={false}
          onChangeText={onChangeInvestment}
          returnKeyType="send"
          textContentType="oneTimeCode"
          style={{
            height: 50,
            width: '100%',
            borderColor: validInvestment ? '#000' : 'red',
            color: '#000',
            borderWidth: 1,
            padding: 10,
            borderRadius: 10
          }}
        />
        <Text style={{marginTop: 10}}>Distance between orders (%)</Text>
        <TextInput
          placeholder={'Distance between orders (%)'}
          value={ordersDistance}
          keyboardType="numeric"
          maxLength={8}
          autoCapitalize={'none'}
          autoCorrect={false}
          onChangeText={onChangeOrdersDistance}
          returnKeyType="send"
          textContentType="oneTimeCode"
          style={{
            height: 50,
            width: '100%',
            borderColor: validOrdersDistance ? '#000' : 'red',
            color: '#000',
            borderWidth: 1,
            padding: 10,
            borderRadius: 10
          }}
        />
        <Text style={{marginTop: 10}}>Sell profit (%)</Text>
        <TextInput
          placeholder={'Sell profit (%)'}
          value={sellProfit}
          keyboardType="numeric"
          maxLength={8}
          autoCapitalize={'none'}
          autoCorrect={false}
          onChangeText={onChangeSellProfit}
          returnKeyType="send"
          textContentType="oneTimeCode"
          style={{
            height: 50,
            width: '100%',
            borderColor: validSellProfit ? '#000' : 'red',
            color: '#000',
            borderWidth: 1,
            padding: 10,
            borderRadius: 10
          }}
        />
        <TouchableOpacity
          onPress={() => {
            if (~~investment < parseFloat(marketMinimums.minNotional) * 2) {
              setTimeout(() => {
                Alert.alert('Invalid input', `You need to invest at least ${parseFloat(marketMinimums.minNotional) * 2} USDT per grid for this market. Decrease grid numbers or increase investment to continue`)
              }, 100)
              return
            }
            createBot(authContext, botsContext, {bot: {market, investment, ordersDistance, sellProfit}}, navigation)
          }}
          activeOpacity={0.8}
          disabled={!validMarket || !validInvestment || !validOrdersDistance || !validSellProfit}
          style={{
            backgroundColor: '#000',
            height: 50,
            width: '100%',
            padding: 10,
            borderRadius: 10,
            justifyContent: 'center',
            flexDirection: 'row',
            marginTop: 10
          }}
        >
          <Text style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
            alignSelf: 'center'
          }}>Create</Text>
        </TouchableOpacity>
      </View>
    </DefaultScreen>
  )
}