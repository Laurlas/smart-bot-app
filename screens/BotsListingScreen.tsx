import * as React from 'react'
import { ScrollView, Text, TouchableOpacity, View } from 'react-native'

import { DefaultScreen } from '../components/DefaultScreen'
import { useContext, useEffect } from 'react'
import { AuthContext } from '../components/Auth/AuthContext'
import { getBots } from '../components/Bots/BotsActions'
import { BotsContext } from '../components/Bots/BotsContext'

function BotsListingItem({bot, navigation}: any) {
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('BotInfo', {id: bot.id})
      }}
      activeOpacity={0.8}
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 10,
        backgroundColor: '#fff',
        padding: 10,
        marginBottom: 10,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        marginLeft: 15,
        marginRight: 15
      }}
    >
      <View style={{flexDirection: 'column'}}>
        <Text style={{fontSize: 18, fontWeight: 'bold', textAlign: 'center'}}>Market:</Text>
        <Text style={{fontSize: 18, textAlign: 'center'}}>{bot.market}</Text>
      </View>
      <View style={{flexDirection: 'column'}}>
        <Text style={{fontSize: 18, fontWeight: 'bold', textAlign: 'center'}}>Investment:</Text>
        <Text style={{fontSize: 18, textAlign: 'center'}}>{bot.investment} USDT</Text>
      </View>
    </TouchableOpacity>
  )
}

export default function BotsListingScreen({navigation}: any) {
  const authContext = useContext(AuthContext) as any
  const botsContext = useContext(BotsContext) as any
  const {state: {bots}} = botsContext
  useEffect(() => {
    getBots(authContext, botsContext)
  }, [])
  return (
    <DefaultScreen navigation={navigation}>
      <ScrollView
        style={{
          flex: 1
        }}>
        <Text
          style={{
            fontSize: 24,
            padding: 10,
            textAlign: 'center',
            fontWeight: 'bold',
            marginLeft: 15,
            marginRight: 15,
            marginBottom: 15
          }}
        >
          Your bots
        </Text>
        {
          bots.length === 0 &&
          <Text style={{textAlign: 'center', margin: 15, color: 'red'}}>You have no bots yet</Text>
        }
        {
          bots.map((bot: any, index: number) => <BotsListingItem bot={bot} navigation={navigation} key={index}/>)
        }
        <View
          style={{
            margin: 15
          }}
        >
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('CreateBot')
            }}
            activeOpacity={0.8}
            style={{
              backgroundColor: '#000',
              height: 50,
              width: '100%',
              padding: 10,
              borderRadius: 10,
              justifyContent: 'center',
              flexDirection: 'row',
              marginTop: 10,
              marginBottom: 100
            }}
          >
            <Text style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
              alignSelf: 'center'
            }}>Create bot</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </DefaultScreen>
  )
}