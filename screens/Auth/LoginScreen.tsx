import { StackScreenProps } from '@react-navigation/stack'
import * as React from 'react'
import {
  NativeSyntheticEvent,
  StyleSheet,
  Text,
  TextInput,
  TextInputChangeEventData,
  TouchableOpacity,
  View
} from 'react-native'

import { RootStackParamList } from '../../types'
import { useContext, useState } from 'react'
import { AuthContext } from '../../components/Auth/AuthContext'
import { login } from '../../components/Auth/AuthActions'

export const normalizeEmail = (value: string) => {
  if (!value) {
    return value
  }

  const trimmedEmail = value.trim()
  return trimmedEmail.toLowerCase()
}

export function emailIsValid(email: string) {
  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)
}

export function passwordIsValid(password: string) {
  return password.length > 7
}

export default function LoginScreen({
                                      navigation
                                    }: StackScreenProps<RootStackParamList, 'Login'>) {
  const authContext = useContext(AuthContext) as any
  const [email, setEmail] = useState('')
  const [emailChanged, setEmailChanged] = useState(false)
  const [password, setPassword] = useState('')
  const [passwordChanged, setPasswordChanged] = useState(false)
  const validEmail = emailIsValid(email)
  const validPassword = passwordIsValid(password)
  const onChangeEmail = (e: NativeSyntheticEvent<TextInputChangeEventData>) => {
    const newEmail = normalizeEmail(e.nativeEvent.text)
    setEmail(newEmail)
    setEmailChanged(true)
  }
  const onChangePassword = (e: NativeSyntheticEvent<TextInputChangeEventData>) => {
    setPassword(e.nativeEvent.text)
    setPasswordChanged(true)
  }
  return (
    <View style={styles.container}>
      <Text
        style={{
          fontSize: 24,
          padding: 10,
          fontWeight: 'bold'
        }}
      >
        Login
      </Text>
      <TextInput
        placeholder={'Email'}
        value={email}
        autoCapitalize={'none'}
        autoCorrect={false}
        onChange={onChangeEmail}
        returnKeyType="send"
        textContentType="oneTimeCode"
        style={{
          height: 50,
          width: '100%',
          borderColor: !emailChanged || validEmail ? '#000' : 'red',
          color: '#000',
          borderWidth: 1,
          padding: 10,
          borderRadius: 10
        }}
      />
      <TextInput
        placeholder={'Password'}
        secureTextEntry={true}
        value={password}
        autoCapitalize={'none'}
        autoCorrect={false}
        onChange={onChangePassword}
        returnKeyType="send"
        textContentType="oneTimeCode"
        style={{
          height: 50,
          width: '100%',
          borderColor: !passwordChanged || validPassword ? '#000' : 'red',
          color: '#000',
          borderWidth: 1,
          padding: 10,
          borderRadius: 10,
          marginTop: 10
        }}
      />
      <TouchableOpacity
        onPress={() => login(authContext, email, password)}
        activeOpacity={0.8}
        disabled={!validEmail || !validPassword}
        style={{
          backgroundColor: '#000',
          height: 50,
          width: '100%',
          padding: 10,
          borderRadius: 10,
          justifyContent: 'center',
          flexDirection: 'row',
          marginTop: 10
        }}
      >
        <Text style={{
          color: '#fff',
          fontSize: 16,
          fontWeight: 'bold',
          alignSelf: 'center'
        }}>Submit</Text>
      </TouchableOpacity>
      <View style={{
        padding: 5
      }}>
        <Text style={{
          alignSelf: 'center',
          fontSize: 14
        }}>No account?</Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('Register')}
        >
          <Text style={{
            alignSelf: 'center',
            fontSize: 16,
            fontWeight: 'bold',
            textDecorationLine: 'underline'
          }}>Register</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  link: {
    marginTop: 15,
    paddingVertical: 15
  },
  linkText: {
    fontSize: 14,
    color: '#2e78b7'
  }
})
