import * as React from 'react'
import { StyleSheet } from 'react-native'
import { View } from 'react-native'
import { DefaultScreen } from '../components/DefaultScreen'

export default function TabTwoScreen() {
  return (
    <DefaultScreen navigation={navigation}>
      <View style={{
        padding: 15,
        flex: 1
      }}>
      </View>
    </DefaultScreen>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%'
  }
})
