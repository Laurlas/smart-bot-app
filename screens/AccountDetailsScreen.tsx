import * as React from 'react'
import { ScrollView, Text, View } from 'react-native'
import { DefaultScreen } from '../components/DefaultScreen'
import { useContext, useMemo } from 'react'
import { AuthContext } from '../components/Auth/AuthContext'
import { BotsContext } from '../components/Bots/BotsContext'
import { getMarketsHoldings } from '../components/Bots/BotsActions'
import { useFocusEffect } from '@react-navigation/native'
import { BotHoldingsChart } from '../components/partials/BotHoldingsChart'

export default function AccountDetailsScreen({navigation}: any) {
  const authContext = useContext(AuthContext) as any
  const botsContext = useContext(BotsContext) as any
  const {state: {balances, loading}} = botsContext
  useFocusEffect(
    React.useCallback(() => {
      getMarketsHoldings(authContext, botsContext)
      const timer = setInterval(() => {
        getMarketsHoldings(authContext, botsContext)
      }, 5000)
      return () => {
        clearInterval(timer)
      }
    }, []))
  const {labels, availableBalances, onOrderBalances, totalValueBalances} = useMemo(() => {
    let availableBalances = []
    let onOrderBalances = []
    let totalValueBalances = []
    let labels = []
    if (balances) {
      labels = balances.map((balance: any) => balance.market)
      availableBalances = balances.map((balance: any) => parseFloat(balance.available))
      onOrderBalances = balances.map((balance: any) => parseFloat(balance.onOrder))
      totalValueBalances = balances.map((balance: any) => (parseFloat(balance.onOrder) + parseFloat(balance.available)) * parseFloat(balance.price))
    }
    return {availableBalances, onOrderBalances, totalValueBalances, labels}
  }, [balances])
  if (loading) {
    return null
  }
  return (
    <DefaultScreen navigation={navigation}>
      <ScrollView
        style={{
          padding: 15,
          flex: 1
        }}>
        <Text
          style={{
            fontSize: 24,
            padding: 10,
            textAlign: 'center',
            fontWeight: 'bold'
          }}
        >
          Account details
        </Text>
        {
          !balances || balances.length === 0
            ?
            <Text style={{textAlign: 'center', marginTop: 15, marginBottom: 15, color: 'red'}}>You have no holdings yet
              on
              Binance.</Text>
            :
            <View>
              <Text style={{textAlign: 'left', marginTop: 15, marginBottom: 5}}>Available holdings</Text>
              <BotHoldingsChart data={availableBalances} labels={labels}/>

              <Text style={{textAlign: 'left', marginTop: 15, marginBottom: 5}}>On order holdings</Text>
              <BotHoldingsChart data={onOrderBalances} labels={labels}/>

              <Text style={{textAlign: 'left', marginTop: 15, marginBottom: 5}}>All holdings in USTD</Text>
              <BotHoldingsChart data={totalValueBalances} labels={labels}/>
            </View>
        }
      </ScrollView>
    </DefaultScreen>
  )
}