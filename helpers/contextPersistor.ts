import SyncStorage from '../helpers/SyncStorage'

const statePersister = (STORAGE_KEY = 'key', initialState = {}) => {
    let state = SyncStorage.get(STORAGE_KEY)
    if (state) {
        state = JSON.parse(state)
        state.errorLogin = false
    } else {
        state = initialState
    }
    return state
}
const reducerPersistor = (STORAGE_KEY: string, reducer: any) => (state: any, action: any) => {
    const newState = reducer(state, action)
    SyncStorage.set(STORAGE_KEY, JSON.stringify(newState))
    return newState
}
export {reducerPersistor, statePersister}