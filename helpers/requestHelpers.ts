import axios from 'axios'
import httpStatus from 'http-status'
import { Alert } from 'react-native'
import { API_URL, clientConfig } from '../config/constants'

export const REQUEST_AUTH_TOKEN = 'REQUEST_AUTH_TOKEN'
export const REQUEST_AUTH_TOKEN_FAILED = 'REQUEST_AUTH_TOKEN_FAILED'
export const refreshToken = async (authContext: any) => {
  try {
    const response = await axios({
        url: `${API_URL}/oauth2/token`,
        method: 'post',
        data: {
          ...clientConfig,
          refresh_token: authContext.state.refresh_token,
          grant_type: 'refresh_token'
        }
      }
    )
    authContext.dispatch({
      type: REQUEST_AUTH_TOKEN,
      payload: response.data
    })
    return response.data
  } catch (e) {
    authContext.dispatch({
      type: REQUEST_AUTH_TOKEN_FAILED
    })
  }
}

export const makeAuthRequest = (data: any, tries: number = 0, newTokenData: any): any => async (authContext: any, showAlert: boolean = true) => {
  try {
    let tokenData = authContext.state
    if (newTokenData) {
      tokenData = newTokenData
    }
    if (tokenData.access_token) {
      data.headers = {Authorization: `${tokenData.token_type} ${tokenData.access_token}`}
    }
    return await axios(data)
  } catch (error) {
    if (error.response) {
      if (error.response.status === httpStatus.extra.iis.LOGIN_TIME_OUT) {
        if (tries === 0) {
          const tokenResponse = await refreshToken(authContext)
          if (tokenResponse) {
            return await makeAuthRequest(data, tries + 1, tokenResponse)(authContext)
          }
        } else {
          authContext.dispatch({
            type: REQUEST_AUTH_TOKEN_FAILED
          })
        }
      } else {
        if (error.response.data) {
          const data = error.response.data
          if (data.errors) {
            if (showAlert && data.errorType === 'other' && data.errors.message) {
              setTimeout(() => {
                Alert.alert('Error', data.errors.message)
              }, 100)
            }
          }
        }
      }
    } else if (error.request) {
      // console.log(error.request)
    } else {
      // console.log('Error', error.message)
    }
  }
}
export const stringifyQuery = (queryParams: any) => {
  const params = []
  for (const param in queryParams) {
    if (queryParams.hasOwnProperty(param)) {
      if (Array.isArray(queryParams[param])) {
        for (const innerIndex in queryParams[param]) {
          if (queryParams[param].hasOwnProperty(innerIndex)) {
            if (Array.isArray(queryParams[param][innerIndex])) {
              for (const innerInnerIndex in queryParams[param][innerIndex]) {
                if (queryParams[param][innerIndex].hasOwnProperty(innerInnerIndex)) {
                  if (!(Array.isArray(queryParams[param][innerIndex][innerInnerIndex])) && !(queryParams[param][innerIndex][innerInnerIndex] instanceof Object)) {
                    params.push(`${param}[${innerIndex}][${innerInnerIndex}]=${queryParams[param][innerIndex][innerInnerIndex]}`)
                  }
                }
              }
            } else if (!(queryParams[param][innerIndex] instanceof Object)) {
              params.push(`${param}[${innerIndex}]=${queryParams[param][innerIndex]}`)
            }
          }
        }
      } else if (queryParams[param] instanceof Object) {
        let index = 0
        for (const innerIndex in queryParams[param]) {
          if (queryParams[param].hasOwnProperty(innerIndex)) {
            // params.push(`${param}[${index}][0]=${innerIndex}`)
            if (Array.isArray(queryParams[param][innerIndex])) {
              for (const innerInnerIndex in queryParams[param][innerIndex]) {
                if (queryParams[param][innerIndex].hasOwnProperty(innerInnerIndex)) {
                  if (!(Array.isArray(queryParams[param][innerIndex][innerInnerIndex])) && !(queryParams[param][innerIndex][innerInnerIndex] instanceof Object)) {
                    params.push(`${param}[${index}][${Number(innerInnerIndex) + 1}]=${queryParams[param][innerIndex][innerInnerIndex]}`)
                  }
                }
              }
            } else if (!(queryParams[param][innerIndex] instanceof Object)) {
              params.push(`${param}[${index}]=${queryParams[param][innerIndex]}`)
            }
          }
          index++
        }
      } else {
        params.push(`${param}=${queryParams[param]}`)
      }
    }
  }
  return params.join('&')
}
