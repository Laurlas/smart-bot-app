import { createDrawerNavigator } from '@react-navigation/drawer'
import React, { useContext, useEffect, useRef, useState } from 'react'
import { OpenSideMenu } from './SideNavigator'
import BottomTabNavigator from './BottomTabNavigator'
import AccountDetailsScreen from '../screens/AccountDetailsScreen'
import { AuthContext } from '../components/Auth/AuthContext'
import { registerForPushNotificationsAsync } from '../helpers/notificationsHelper'
import { registerNotificationToken } from '../components/Auth/AuthActions'
import * as Notifications from 'expo-notifications'
import { Alert } from 'react-native'

const Drawer = createDrawerNavigator()
export default function MyDrawer() {
  const [initRender, setInitRender] = useState(true)
  const authContext = useContext(AuthContext) as any
  const notificationListener = useRef()
  const responseListener = useRef() as any
  useEffect(() => {
    registerForPushNotificationsAsync().then(token => registerNotificationToken({authContext, token}))

    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      Alert.alert(notification?.request?.content?.title || '', notification?.request?.content?.body || '')
    }) as any

    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
    })

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current as any)
      Notifications.removeNotificationSubscription(responseListener.current)
    }
  }, [])
  useEffect(() => {
    setInitRender(false)
  }, [initRender])

  return (
    <Drawer.Navigator
      initialRouteName={'Main'}
      screenOptions={{headerShown: false}}
      drawerStyle={{overflow: 'hidden', width: '70%'}}
      drawerContent={({navigation}) => <OpenSideMenu navigation={navigation}/>}>
      <Drawer.Screen
        component={BottomTabNavigator} name={'Main'}
        options={{
          gestureEnabled: true
        }}
      />
      <Drawer.Screen
        component={AccountDetailsScreen} name={'Account details'}
        options={{
          gestureEnabled: true
        }}
      />
    </Drawer.Navigator>
  )
}

