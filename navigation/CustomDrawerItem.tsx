import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'

export const CustomDrawerItem = ({
                                   keyItem,
                                   label,
                                   focused,
                                   onPress,
                                   icon,
                                   labelStyle
                                 }: any) => {
  const iconNode = icon ? icon({size: 50, focused}) : null
  return (
    <TouchableOpacity onPress={onPress} key={keyItem} style={{flexDirection: 'row'}}
                      activeOpacity={0.8}>
      <React.Fragment>
        <View style={{alignSelf: 'center', marginLeft: 10}}>
          {iconNode}
        </View>
        <View style={{
          flex: 1,
          justifyContent: 'center',
          alignSelf: 'center',
          paddingTop: 15,
          paddingBottom: 15,
          borderBottomColor: '#EAEDF2',
          borderBottomWidth: 0.5,
          marginLeft: 20
        }}>
          <Text style={[{...labelStyle}, {
            color: focused ? 'red' : 'blue',
            fontSize: 16,
            alignSelf: 'center'
          }]}>
            {label}
          </Text>
        </View>
      </React.Fragment>
    </TouchableOpacity>
  )
}
