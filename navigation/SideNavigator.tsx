import React, { useContext } from 'react'
import { SafeAreaView, Text, TouchableOpacity, View } from 'react-native'
import { AuthContext } from '../components/Auth/AuthContext'
import { logout } from '../components/Auth/AuthActions'

export const OpenSideMenu = ({navigation}: any) => {
  const authContext = useContext(AuthContext)
  return (
    <>
      <SafeAreaView
        style={{
          flex: 1,
          width: '100%',
          paddingTop: 50,
          paddingBottom: 50
        }}
      >
        <View
          style={{
            width: '100%',
            flex: 1,
            padding: 0, margin: 0,
            paddingTop: 0
          }}
        >
          <TouchableOpacity
            onPress={() => navigation.navigate('Bots')}
            style={{flexDirection: 'row'}}
            activeOpacity={0.8}
          >
            <View style={{
              flex: 1,
              justifyContent: 'center',
              alignSelf: 'center',
              paddingTop: 15,
              paddingBottom: 15,
              borderBottomColor: '#EAEDF2',
              borderBottomWidth: 0.5
            }}>
              <Text style={{
                color: '#000',
                fontSize: 16,
                alignSelf: 'center'
              }}>
                Bots
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('Account details')}
            style={{flexDirection: 'row'}}
            activeOpacity={0.8}
          >
            <View style={{
              flex: 1,
              justifyContent: 'center',
              alignSelf: 'center',
              paddingTop: 15,
              paddingBottom: 15,
              borderBottomColor: '#EAEDF2',
              borderBottomWidth: 0.5
            }}>
              <Text style={{
                color: '#000',
                fontSize: 16,
                alignSelf: 'center'
              }}>
                Account details
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => logout({authContext})}
            style={{flexDirection: 'row'}}
            activeOpacity={0.8}
          >
            <View style={{
              flex: 1,
              justifyContent: 'center',
              alignSelf: 'center',
              paddingTop: 15,
              paddingBottom: 15,
              borderBottomColor: '#EAEDF2',
              borderBottomWidth: 0.5
            }}>
              <Text style={{
                color: '#000',
                fontSize: 16,
                alignSelf: 'center'
              }}>
                Logout
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </>
  )
}
