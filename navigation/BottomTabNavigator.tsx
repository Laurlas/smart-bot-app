import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import * as React from 'react'

import { TabOneParamList, TabTwoParamList } from '../types'
import ApiSettingsScreen from '../screens/ApiSettingsScreen'
import BotsListingScreen from '../screens/BotsListingScreen'
import CreateBotScreen from '../screens/CreateBotScreen'
import BotInfoScreen from '../screens/BotInfoScreen'

import LottieView from 'lottie-react-native'
import { useEffect, useState } from 'react'

const BottomTab = createMaterialBottomTabNavigator()

export default function BottomTabNavigator() {
  return (
    <BottomTab.Navigator
      initialRouteName="Bots"
      barStyle={{backgroundColor: '#fff'}}
      shifting={false}
    >
      <BottomTab.Screen
        name="Bots"
        component={TabOneNavigator}
        options={{
          tabBarIcon: (props) => <TabBarButton
            {...props}
            speed={0.5}
            icon={require('../assets/lottie/19872-cpu-crypto-loading-animation.json')}
          />
        }}
      />
      <BottomTab.Screen
        name="Binance Api Settings"
        component={TabTwoNavigator}
        options={{
          tabBarIcon: (props) => <TabBarButton {...props}
                                               icon={require('../assets/lottie/38199-settings-roll.json')}
          />
        }}

      />
    </BottomTab.Navigator>
  )
}

const TabOneStack = createStackNavigator<TabOneParamList>()

function TabOneNavigator() {
  return (
    <TabOneStack.Navigator
      screenOptions={{headerShown: false}}
    >
      <TabOneStack.Screen
        name="Bots"
        component={BotsListingScreen}
        options={{headerTitle: 'Bots'}}
      />
      <TabOneStack.Screen
        name="CreateBot"
        component={CreateBotScreen}
        options={{headerTitle: 'Create Bot'}}
      />
      <TabOneStack.Screen
        name="BotInfo"
        component={BotInfoScreen}
        options={{headerTitle: 'View Bot'}}
      />
    </TabOneStack.Navigator>
  )
}

const TabTwoStack = createStackNavigator<TabTwoParamList>()

function TabTwoNavigator() {
  return (
    <TabTwoStack.Navigator
      screenOptions={{headerShown: false}}
    >
      <TabTwoStack.Screen
        name="Binance Api Settings"
        component={ApiSettingsScreen}
      />
    </TabTwoStack.Navigator>
  )
}

const TabBarButton = (props: any) => {
  const [animation, setAnimation] = useState<any>()
  useEffect(() => {
    props.focused && animation && animation.play()
  }, [props])
  return (
    <LottieView
      style={{height: 30}}
      source={props.icon}
      loop={false}
      speed={props.speed || 1}
      ref={anim => {
        setAnimation(anim)
      }}
    />
  )
}