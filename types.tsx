/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

export type RootStackParamList = {
  Login: undefined;
  Register: undefined;
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Bots: undefined;
  CreateBot: undefined;
  BotInfo: undefined;
  "Binance Api Settings": undefined;
};

export type TabOneParamList = {
  Bots: undefined;
  CreateBot: undefined;
  BotInfo: undefined;
};

export type TabTwoParamList = {
  "Binance Api Settings": undefined;
};
